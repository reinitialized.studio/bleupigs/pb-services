--[[
    PigBot-Core
    Foundation and Initializer for PigBot

    PigBot-Core is the Foundation for PigBot Components and the first-initialized
    file in the PigBot Initialization Process. Using a new OOP-orientiated concept,
    PigBot-Core rebuilds the PigBot system into a much more organized and maintainable
    structure.

    Changelog:
        [3252018]
            -   initial publish
]]
local ran, failureResponse = coroutine.resume(coroutine.create(function()
------  PRE-INITIALIZATION 
----  variable localization
local newproxy = newproxy
local getmetatable = getmetatable
local setmetatable = setmetatable
local coroutine = coroutine
local resumeThread = coroutine.resume
local yieldRunningThread = coroutine.yield
local getRunningThread = coroutine.running
local getThreadStatus = coroutine.status
local createThread = coroutine.create
local debug = debug
local getTraceback = debug.traceback
local string = string
local formatString = string.format
local gsubString = string.gsub
local matchString = string.match
local lowerString = string.lower
local math = math
local generateRandom = math.random
local os = os
local getTime = os.time
local exitProcess = os.exit
local assert = assert
local typeof = type
local type = type
local shared = shared
local error = error
local debug = debug
local require = require
local pcall = pcall
local getfenv = getfenv
local setfenv = setfenv
local environment = getfenv()
----  environment variables
local fsRoot = "./src/"
local arguments = rawget(   
	_G,   
	"args" 
) or {}
local totalComponents
local totalThreads
local defaultEnvironment
local lookupComponents
local lookupThreadToSession
local rootThread = getRunningThread()
--  library initialization
local fsys = require("coro-fs")
local http = require("coro-http")
local sleep = require("timer").sleep
local json = require("json")
----  load permissions        
local objectPermissions do   
	local res, data = http.request(
		"GET",
		"https://gitlab.com/bleu-pigs/".. arguments[3] .."/raw/".. arguments[2] .."/src/permissions.json"
	)
	objectPermissions = json.decode(data)
end
local botConfig = json.decode(data)
local PBInfo = {
	deployType = arguments[2],
	project = arguments[3],
	branch = arguments[4]
}
debug(
	formatString(
		"initializing %s %s %s",
		PBInfo.project,
		PBInfo.branch,
		PBInfo.deployType
	)
)
--  debug debug
function environment.debug(message, ...)
	if (PBInfo.branch:sub(0, 5) == "indev") then
		local arguments = {...}
		for index = 1, #arguments do
			arguments[index] = tostring(arguments[index])
		end
		--  collect information
		local traceback = getTraceback()
		local stackInformation = getStackInfo(3)
		local runningThread = getRunningThread()
		local writeTime = getTime()
		message = formatString(
			message,
			unpack(arguments)
		)
		debug(
			formatString(
				"[DEBUG INFO for %s]\ntraceback: %s\ncalling function (may be nil): %s\n\n%s\n\n%s",
				runningThread,
				traceback,
				stackInfo.name,
				message,
				writeTime
			)
		)
	end
end
--  load authentication data
do  --  load and merge separate botAuthentication into botConfig
	local botAuthentication, failure = fsys.readFile(--  unforunately, still have to keep you on disk.
		"./botAuthentication.json",
		0666
	)
	if (failure ~= nil) then
		error("failed to load botAuthentication >>\n".. failure)
	end
	botAuthentication = json.decode(botAuthentication)
	if (PBInfo.branch == "master") then
		botAuthentication = botAuthentication.production
	else
		botAuthentication = botAuthentication.test
	end
	for key, value in next, botAuthentication do
		botConfig[key] = value
	end
end
--  utilities
local function assign(target, ...)
	for i = 1, select("#", ...) do
		for key, value in next, select(i, ...) do
			target[key] = value
		end
	end
	return target
end 
----  @classdef  ObjectsManager
----  @desc  Singleton which helps register, create, and manage custom Objects
local TOSTRING_DEFAULT = "Object %s"
local METATABLE_DEFAULT = "Metatable is locked"
local ObjectClassRegister = newproxy()
local InstanciatedObjectRegister = newproxy()
local __private_ObjectsManager = {}
local ObjectsManager = newproxy(true) do
	------  PRE-INITIALIZATION
	----  initializing ObjectsManager data
	local fakeObject = newproxy(true)
	local fakeObjectMetatable = getmetatable(fakeObject)
	local fakeRoot = {}
	fakeObjectMetatable.__index = fakeRoot
	fakeObjectMetatable.__newindex = fakeRoot
	fakeObjectMetatable.__metatable = false
	__private_ObjectsManager.realObject = ObjectsManager
	__private_ObjectsManager.object = fakeObject
	__private_ObjectsManager.objectClassname = "ObjectsManager"
	__private_ObjectsManager.objectTostring = formatString(TOSTRING_DEFAULT, __private_ObjectsManager.objectClassname)
	__private_ObjectsManager.objectMetatable = METATABLE_DEFAULT
	__private_ObjectsManager.objectRoot = {}
	local root = __private_ObjectsManager.objectRoot
	local metatable = __private_ObjectsManager.objectMetatable
	----  metatable initialization
	metatable.__index = root
	metatable.__newindex = root
	metatable.__tostring = function()
		return __private.objectTostring
	end
	metatable.__metatable = false
	------  INITIALIZATION
	----  ObjectsManager setup
	--  cache  totalObjectClass
	--  Weak-reference to all available ObjectClasses
	local totalObjectClass = setmetatable(
		{},
		{
			__mode = "v"
		}
	)  __private_ObjectsManager.totalObjectClass = totalObjectClass
	--  cache  totalObject
	--  Weak-reference to all available Objects
	local totalObject = setmetatable(
		{},
		{
			__mode = "v"
		}
	)  __private_ObjectsManager.totalObject = totalObject
	--  lookup  lookupAny
	--  Used to lookup any Objects or ObjectClasses within Construct
	local lookupAny = setmetatable(
		{},
		{
			__mode = "k",
			__newindex = function(self, index, value)
				if (value.type == ObjectClassRegister) then
					--  created a new ObjectClass
					totalObjectClass[#totalObjectClass + 1] = value
				elseif (value.type == InstanciatedObjectRegister) then
					--  created a new Object
					totalObject[#totalObject + 1] = value
				end
				rawset(
					self,
					index,
					value
				)
			end
		}
	)  __private_ObjectsManager.lookupAny = lookupAny
	----  API declaration
	--  @param  Object Object
	--  @desc  Checks to see if Object is managed by ObjectsManager
	--  @returns  boolean
	local function isManaged(Object)
		local __type_Object = type(Object)
		if (__type_Object ~= "userdata") then
			return error(__type_Object == "userdata", "bad argument #1 (expected userdata, got ".. __type_Object ..")")
		end
		local __private_Object = (lookupAny[Object] or lookupAny[Object.classname])
		if (__private_Object ~= nil) then
			local type = __private_Object.type
			if (type == InstanciatedObjectRegister) then
				return (lookupAny[Object] ~= nil)
			elseif (type == ObjectClassRegister) then
				return (lookupAny[Object.classname] ~= nil)
			end
		end
		return false
	end  ObjectsManager.isManaged = isManaged
	--  @param  string newClassname,  ObjectClass extendingClass
	--  @desc  Registeres a new Class which can be created via ObjectsManager
	--  @returns  Object
	function ObjectsManager.newObjectClass(newClassname, extendingClass)
		local __type_newClassname = type(newClassname)
		local __type_extendingClass = type(extendingClass)
		if not (__type_newClassname == "string") then
			return error("bad argument #1 (expected string, got ".. __type_newClassname ..")")
		end
		if (__type_extendingClass ~= "nil") then
			if (__type_extendingClass ~= "userdata") then
				return error("bad argument #2 (expecting userdata, got ".. __type_extendingClass ..")")
			end
			--  ensure its a valid Construct Object
			if (isManaged(extendingClass) == false) then
				return error("bad argument #2 (expecting Object, got ".. __type_extendingClass ..")")
			end
		end
		--  has this ObjectClass been created before?
		assert(lookupAny[newClassname] == nil, "bad argument #1 (ClassName ".. newClassname .." has already been registered!)")
		------  FUNCTION LOGIC
		local newObjectClass = newproxy(true)
		local __private_newObjectClass = {
			class = newObjectClass,
			classClassname = newClassname,
			classTostring = formatString(TOSTRING_DEFAULT, newClassname),
			classMetatable = getmetatable(newObjectClass),
			classRoot = {
				classname = newClassname
			},
			type = ObjectClassRegister
		}  lookupAny[newClassname] = __private_newObjectClass
		lookupAny[newObjectClass] = __private_newObjectClass
		local root, metatable = __private_newObjectClass.classRoot, __private_newObjectClass.classMetatable
		----  metatable config
		if (extendingClass ~= nil) then
			--  implement config using inherit
			__private_newObjectClass.extendingClass = extendingClass
			local __private_extendingClass = lookupAny[extendingClass.classname]
			__private_newObjectClass.extendingClassPrivate = __private_extendingClass
			--  implement onCreate
			if (__private_extendingClass.onCreate ~= nil) then
				__private_newObjectClass.onCreate = __private_extendingClass.onCreate
			end
			function metatable:__index(index)
				local result = root[index]
				if result == nil then
					result = __private_newObjectClass.extendingClass[index]
					root[index] = result
				end
				return result
			end
		else
			metatable.__index = root
		end
		metatable.__newindex = root
		function metatable.__tostring()
			return __private_newObjectClass.classTostring
		end
		metatable.__metatable = METATABLE_DEFAULT
		------  FINISH
		root.name = __private_newObjectClass.classClassname
		debug("created ObjectClass ".. root.name)
		return newObjectClass
	end
	--  @param  string classname,  string name
	--  @desc  Creates a new Object of classname. Will optionally name as name if available,
	--  otherwise defaults to className
	--  @returns  Object
	function ObjectsManager.newObject(classname, name, ...)
		local __type_classname = type(classname)
		local __type_name = type(name)
		assert(__type_classname == "string", "bad argument #1 (expecting string, got ".. __type_classname ..")")
		if __type_name ~= "nil" then
			assert(__type_name == "string", "bad argument #1 (expecting string, got ".. __type_classname ..")")
		else
			name = classname
		end
		assert(lookupAny[classname], "ObjectClass \"".. classname .."\" does not exist")
		------  OBJECT-INSTANICATING LOGIC
		local __private_ClassObject = lookupAny[classname]
		local ClassObject = __private_ClassObject.class
		local newObject = newproxy(true)
		local __private_newObject = {
			object = newObject,
			objectClassname = __private_ClassObject.classClassname,
			objectTostring = __private_ClassObject.classTostring,
			objectMetatable = getmetatable(newObject),
			objectRoot = {
				classname = __private_ClassObject.classClassname
			},
			objectId = #totalObject,
			ClassObject = ClassObject,
			ClassPrivate = __private_ClassObject,
			type = InstanciatedObjectRegister
		}
		lookupAny[newObject] = __private_newObject
		local root, metatable = __private_newObject.objectRoot, __private_newObject.objectMetatable
		--	initialize metatable
		function metatable:__index(index)
			local result = root[index]
			if (result == nil) then
				result = ClassObject[index]
				root[index] = result
			end
			return result
		end 
		metatable.__newindex = root
		function metatable.__tostring()
			return __private_newObject.objectTostring
		end
		metatable.__metatable = METATABLE_DEFAULT
		--  setup onCreate
		root.name = name
		if (__private_ClassObject.onCreate ~= nil) then
			__private_ClassObject.onCreate(
				newObject,
				...
			)
		end
		----  FINISH
		debug("created Object", root.classname, root.name)
		return newObject
	end
	-- ----  @ref  ObjectsManager.__private  -- removing this link in favor of a controlled executable
	-- ObjectsManager.__private = __private
end
local lookupAny = __private_ObjectsManager.lookupAny
----  permission utilities
local function getPermissions(Object)
	assert(ObjectsManager.isManaged(Object))
	local __private_Object = (lookupAny[Object] or lookupAny[Object.classname])
	debug("a", Object.classname, Object.name, __private_Object)
	local permissions = objectPermissions.objects[__private_Object.objectId] or objectPermissions.classes[__private_Object.objectClassname] or objectPermissions.classes[__private_Object.classClassname]
	while (permissions == nil) do
		if (__private_Object.ClassPrivate ~= nil) then
			__private_Object = __private_Object.ClassPrivate
			if __private_Object.realObject then
				return {
					permissions = objectPermissions.default.permissions,
					protected = objectPermissions.default.protected
				}
			end
			permissions = objectPermissions.classes[__private_Object.objectClassname] or objectPermissions.classes[__private_Object.classClassname]
		else
			break
		end
	end
	return permissions or {
		permissions = objectPermissions.default.permissions,
		protected = objectPermissions.default.protected
	}
end
local newObject = ObjectsManager.newObject
local newObjectClass = ObjectsManager.newObjectClass
----  @classdef  Object
----  @desc  Abstract Base for all defined ObjectClasses
local classdefObject = ObjectsManager.newObjectClass("Object") do
	------  CLASS PRE-INITIALIZATION
	local __private_Object = lookupAny.Object
	local lookupProxiedFunction = setmetatable(
		{},
		{
			__mode = "v"
		}
	)
	local ACCESS_DENIED_STRING = "ACCESS DENIED: "
	local ACCESS_DENIED_PERMISSION = ACCESS_DENIED_STRING .."Incorrect Permissions\nRequest: %s\nRequest Permissions: %s\nRequired Permissions: %s"
	----  OnCreate
	function __private_Object:onCreate()
		local __private_self = lookupAny[self]
		----  metatable permission-check modifications
		local metatable = __private_self.objectMetatable
		local root = __private_self.objectRoot
		--  Override Index
		function metatable:__index(index)
			if (ObjectsManager.isManaged(self) == false) then
				if (isGranted == false)  then
					if (PBInfo.deployType == "test") then
						return error(ACCESS_DENIED_STRING .."self is not Managed")
					end
					return
				end
			end
			local requestingSession = lookupThreadToSession[getRunningThread()]
			if (requestingSession == nil) then
				if (isGranted == false)  then
					if (PBInfo.deployType == "test") then
						return error(ACCESS_DENIED_STRING .."no session")
					end
					return
				end
			end
			local indexLowered = lowerString(index)
			local __private_self = lookupAny[self]
			if (indexLowered == "classname" or indexLowered == "name") then
				return (root[index] or __private_self.ClassObject[index])
			end 
			local __private_requestingSession = lookupAny[requestingSession]
			local requestingComponent = __private_requestingSession.component
			local __private_requestingComponent = lookupAny[requestingComponent]
			local requestingPermissions = __private_requestingSession.permissions
			local enforcedPermissions = getPermissions(self)
			local isGranted = false
			----  perform permission checks
			local requesting = root[index] or __private_self.ClassObject[index]
			if (type(requesting) == "function") then
				local requestingFunction = lookupProxiedFunction[requesting]
				if (requestingFunction == nil) then
					requestingFunction = setfenv(
						function(...)
							enforcedPermissions = getPermissions(self)
							enforcedPermissions = enforcedPermissions.protected[indexLowered .."-execute"] or enforcedPermissions.permissions.execute
							debug("running execute permissions check on ".. __private_self.objectRoot.name  .." >>\nIndex: ".. indexLowered .."\nRequestType: Execute\nRequestIdentity: ".. requestingPermissions.execute .."\nRequiredIdentity: ".. enforcedPermissions .."\nObjectId: ".. __private_self.objectId .."\nRequesting: ".. __private_requestingComponent.componentName)
							isGranted = (requestingPermissions.execute >= enforcedPermissions)
							if (isGranted == false)  then
								if (PBInfo.deployType == "test") then
									return error(
										formatString(
											ACCESS_DENIED_PERMISSION,
											"execute",
											requestingPermissions.execute,
											enforcedPermissions
										)
									)
								end
								return
							end
							return requesting(...)
						end,
						__private_requestingSession.environment
					)
					lookupProxiedFunction[requesting] = requestingFunction
				end
				return requestingFunction
			else
				enforcedPermissions = enforcedPermissions.protected[indexLowered .."-read"] or enforcedPermissions.permissions.read
				debug("running __index permissions check on ".. __private_self.objectRoot.name  .." >>\nIndex: ".. indexLowered .."\nRequestType: Read\nRequestIdentity: ".. requestingPermissions.read .."\nRequiredIdentity: ".. enforcedPermissions .."\nObjectId: ".. __private_self.objectId .."\nRequesting: ".. __private_requestingComponent.componentName)
				isGranted = (requestingPermissions.read >= enforcedPermissions)
				if (isGranted == false) then
					if (PBInfo.deployType == "test") then
						return error(
							formatString(
								ACCESS_DENIED_PERMISSION,
								"read",
								requestingPermissions.read,
								enforcedPermissions
							)
						)
					end
					return
				end
			end
			return requesting
		end
		function metatable:__newindex(index, value)
			if (ObjectsManager.isManaged(self) == false) then
				if (isGranted == false)  then
					if (PBInfo.deployType == "test") then
						return error(ACCESS_DENIED_STRING .."self is not Managed")
					end
					return
				end
			end
			local requestingSession = lookupThreadToSession[running()]
			if (requestingSession == nil) then
				if (isGranted == false)  then
					if (PBInfo.deployType == "test") then
						return error(ACCESS_DENIED_STRING .."no session")
					end
					return
				end
			end
			local indexLowered = lowerString(index)
			local __private_requestingSession = lookupAny[requestingSession]
			local requestingComponent = __private_requestingSession.component
			local __private_requestingComponent = lookupAny[requestingComponent]
			local __private_self = lookupAny[self]
			local requestingPermissions = __private_requestingSession.permissions
			local enforcedPermissions = getPermissions(self)
			local isGranted = false
			----  perform permission checks
			enforcedPermissions = enforcedPermissions.protected[index .."-write"] or enforcedPermissions.permissions.write
			debug("running __newindex permissions check on ".. __private_self.objectRoot.name  .." >>\nIndex: ".. indexLowered .."\nRequestType: Write\nRequestIdentity: ".. __private_requestingComponent.write .."\nRequiredIdentity: ".. enforcedPermissions .."\nObjectId: ".. __private_self.objectId .."\nRequesting: ".. requestingComponentPrivate.componentName)
			isGranted = (requestingPermissions.write >= enforcedPermissions)
			if (isGranted == false)  then
				return error(
					formatString(
						ACCESS_DENIED_PERMISSION,
						"write",
						requestingPermissions.write,
						enforcedPermissions
					)
				)
			end
			----  basic security checks passed, perform the action
			root[index] = value 
		end
	end
	------  CLASS LOGIC
	--	@param  none
	--  @desc  Returns what the Object inherits from
	--  @returns  string
	function classdefObject:getInheritsFrom()
		return lookupAny[self].ClassObjectPrivate.classTostring
	end
	--  @param  string isA
	--  @desc  Returns boolean whether Object is or isn't a inheritent of Class isA
	--  @returns  boolean
	function classdefObject:isA(isA)
		local lastSearched = lookupAny[self]
		if (
			matchString(
				lastSearched.objectClassname:sub(1, #isA),
				isA
		) ~= true) then
			while true do
				if lastSearched.classClassname:sub(1, #isA):match(isA) then
					return true
				end
				if lastSearched == __private_ObjectsManager then
					break
				end
				lastSearched = lastSearched.ClassObjectPrivate
			end
		end
		return false
    end
    --  @param  none
    --  @desc  Clones the Object
	--  @returns  Object
	local dontAssign = {
		object = true,
		objectClassname = true,
		objectTostring = true,
		objectMetatable = true,
		objectRoot = true,
		ClassObject = true,
		ClassPrivate = true,
		type = true,
		class = true,
		classClassname = true,
		classTostring = true,
		classMetatable = true,
		classRoot = true
	}
    function classdefObject:clone()
		-- local __private_self = lookupAny[self]
		-- local newObject = ObjectsManager.newObject(
		-- 	self.classname,
		-- 	self.name
		-- )
		-- local __private_newObject = lookupAny[self]
		-- for key, value in next, __private_self do
		-- 	if (dontAssign[key] == nil) then
		-- 		__private_newObject[key] = value
		-- 	end
		-- end
		-- return newObject
		error("not implemented")
    end
	--  @param  none
	--  @desc  Destroys the Object and all of its associated data
	--  @returns  none
	function classdefObject:destroy()
		local __private_self = lookupAny[self]
		if (__private_self.type == InstanciatedObjectRegister) then
			lookupAny[self] = nil
			for key in next, __private_self.objectRoot do
				__private_self.objectRoot[key] = nil
			end
			for key in next, __private_self.objectMetatable do
				__private_self.objectMetatable[key] = nil
			end
			for key in next, __private_self do
				__private_self[key] = nil
			end
		elseif (__private_self.type == ObjectClassRegister) then
			lookupAny[__private_self.classClassname] = nil
			for key in next, __private_self.classRoot do
				__private_self.classRoot[key] = nil
			end
			for key in next, __private_self.classMetatable do
				__private_self.classMetatable[key] = nil
			end
			for key in next, __private_self do
				__private_self[key] = nil
			end
		end
	end
end
------  INITIALIZATION
----  @classdef  PBCore
----  @desc  Root of PBCore and its APIs
local classdefPBCore = ObjectsManager.newObjectClass(
	"PBCore", 
	classdefObject
)
local __private_PBCore = lookupAny.PBCore
classdefPBCore.Info = PBInfo
----  @classdef  Component
----  @desc  Abstract Class representing all Components for PigBot
local classdefComponent = ObjectsManager.newObjectClass(
	"Component", 
	classdefObject
) do
	local __private_Component = lookupAny.Component
	------  INITIALIZATION
	----  variable setup
	--  reference for all in-memory components
	totalComponents = setmetatable(
		{},
		{
			__mode = "v"
		}
	)  __private_PBCore.totalComponents = totalComponents
	--  lookup for initialized PigBot Components
	lookupComponent = setmetatable(
		{},
		{
			__mode = "k",
			__newindex = function(this, index, value)
				totalComponents[#totalComponents + 1] = value
				rawset(
					this,
					index,
					value
				)
			end
		}
	)  __private_PBCore.lookupComponent = lookupComponent
	--  reference to all threads created and managed by PigBot
	totalThreads = setmetatable(
		{},
		{
			__mode = "v"
		}
	)  __private_PBCore.totalThreads = totalThreads
	--  lookup to the associated session for each thread
	lookupThreadToSession = setmetatable(
		{},
		{
			__mode = "k"
		}
	)  __private_PBCore.lookupThreadToSession = lookupThreadToSession
	--  default master environment for all Components
	defaultEnvironment = setmetatable(
		{
		},
		{
			__index = environment
		}
	) __private_PBCore.defaultEnvironment = defaultEnvironment
	----  internal Component Classes
	----  @classdef  Session
	----  @desc  Object which represents an identity for attached threads.
	local classdefSession = ObjectsManager.newObjectClass(
		"Session", 
		classdefObject
	) do
		local __private_Session = lookupAny.Session
		------  INITIALIZATION
		----  variable declaration
		------  DEFINITION
		----  API declaration
		--  @param  Object self, Object Component
		--  @desc  Initializes a Session Object for use to track and identify the threads of a Component
		function __private_Session:onCreate(Component)
			assert(ObjectsManager.isManaged(Component), "bad argument #1 (expecting Object, got ".. type(Component) ..")")
			------  INITIALIZATION
			----  variable declaration
			lookupAny.Object.onCreate(self)
			local __private_self = lookupAny[self]
			__private_self.component = Component
			__private_self.componentPrivate = lookupAny[Component]
			__private_self.componentPrivate.session = self
			__private_self.sessionId = "Session".. generateRandom(1000) .. generateRandom(1000) .. generateRandom(1000)
			__private_self.environment = setmetatable(
				{
					ready = function()
						__private_self.environment.ready = nil
						debug(__private_self.component.name, "ready")
					end,
					component = Component,
					root = __private_self.componentPrivate.componentRoot,
					objectData = __private_ObjectsManager,
					coroutine = {
						create = function(providedFunction)
							local __type_providedFunction = type(providedFunction)
							if __type_providedFunction ~= "function" then
								error(
									formatString(
										"bad argument #1 (expected function, got %s)",
										__type_providedFunction
									),
									2
								)
							end
							----  main function logic
							local newThread = createThread(providedFunction)
							local session = lookupThreadToSession[getRunningThread()]
							if (session ~= nil) then
								session:attachThread(newThread)
							end
							return newThread
						end,
						wrap = function(providedFunction)
							local __type_providedFunction = type(providedFunction)
							if __type_providedFunction ~= "function" then
								error(
									formatString(
										"bad argument #1 (expected function, got %s)",
										__type_providedFunction
									),
									2
								)
							end
							----  main function logic
							local newThread = createThread(providedFunction)
							local session = lookupThreadToSession[getRunningThread()]
							if (session ~= nil) then
								session:attachThread(newThread)
								return setfenv(
									function(...)
										return __private_self.environment.coroutine.resume(
											newThread,
											...
										)
									end,
									lookupAny[session].environment
								)
							end
							return function(...)
								return __private_self.environment.coroutine.resume(
									newThread,
									...
								)
							end
						end,
						resume = function(threadToResume, ...)
							local ran, result = resumeThread(
								threadToResume, 
								...
							)
							if (ran == false) then
								local errorStringFormatted = formatString(
									"[ERROR @ %s]\n%s\nTRACEBACK: %s",
									getTime(),
									result,
									getTraceback()
								)
								return error(errorStringFormatted)
							end
							return result
						end,
						running = threadRunning,
						status = threadStatus,
						yield = yieldRunningThread
					},
				},
				{
					__index = defaultEnvironment,
					__metatable = METATABLE_DEFAULT
				}
			) __private_self.environment.environment = __private_self.environment
			--  get user permissions
			__private_self.permissions = objectPermissions.modules[__private_self.componentPrivate.componentName:lower()] or {
				read = objectPermissions.default.read,
				write = objectPermissions.default.write,
				execute = objectPermissions.default.execute
			}
			-- __private_self.environment.permissions = objPrivate.permissions -- TEMPORARY DEBUGGING
			__private_self.activeThreads = setmetatable(
				{},
				{
					__mode = "v",
					__newindex = function(self, index, value)
						lookupThreadToSession[value] = __private_self.object
						rawset(
							self,
							index,
							value
						)
					end
				}
			)
		end
		--  @param  coroutine thread
		--  @desc  adds the provided thread to the Session Object
		--  @returns  coroutine
		--  @permissions:
		--  read:  3 (SYSTEM)
		--  write:  4 (NONWRITABLE)
		--  execute: 3 (SYSTEM)
		function classdefSession:attachThread(thread)
			assert(type(thread) == "thread", "bad argument #1 (expected thread, got ".. type(thread) ..")")
			if (lookupThreadToSession[thread] ~= nil) then
				error("bad argument #1 (thread already registered to ".. lookupAny[lookupThreadToSession[thread]].sessionId)
			end
			local activeThreads = self:getActiveThreads()
			setfenv(
				3,
				self:getEnvironment()
			)
			return thread
		end
		--  @param  coroutine thread
		--  @desc  removes the provided thread from the Session Object
		--  @returns  coroutine
		--  @permissions:
		--  read:  3 (SYSTEM)
		--  write:  4 (NONWRITABLE)
		--  execute: 3 (SYSTEM)
		function classdefSession:deattachThread(thread)
			assert(type(thread) == "thread", "bad argument #1 (expected thread, got ".. type(thread) ..")")
			assert(lookupThreadToSession[thread], "bad argument #1 (thread already not registered to ".. lookupThreadToSession[thread].sessionId)
			local activeThreads = self:getActiveThreads()
			for key, thread2 in next, activeThreads do
				if (thread == thread2) then
					activeThreads[key] = nil
					lookupThreadToSession[thread] = nil
					break
				end
			end
			return thread
		end 
		--  @param  none
		--  @desc  returns a proxy for processing permissions
		--  @return  userdata
		--  @permissions:
		--  read:  2 (SERVICE)
		--  write:  4 (NONWRITABLE)
		--  execute: 2 (SERVICE)
		function classdefSession:getPermissions()
			return lookupAny[self].permissions
			-- local proxy = newproxy(true)
			-- local metatable = getmetatable(proxy)
			-- metatable.__index = __private_self.permissions
			-- metatable.__newindex = {}
			-- metatable.__metatable = false
			-- return proxy
		end
		--  @desc  returns the activeThreads of the Session
		function classdefSession:getActiveThreads()
			return lookupAny[self].activeThreads
		end
		--  @param  none
		--  @desc  returns the Environment of the Session
		--  @returns  table
		--  @permissions:
		--  read:  3 (SYSTEM)
		--  write:  4 (NONWRITABLE)
		--  execute: 3 (SYSTEM)2
		function classdefSession:getEnvironment()
			return lookupAny[self].environment
		end
		--  @param  none
		--  @desc  returns the 
		--  @param  none
		--  @desc  Returns the Component associated with the Session
		--  @returns  Object
		--  @permissions:
		--  read:  3 (SYSTEM)
		--  write:  4 (NONWRITABLE)
		--  execute: 3 (SYSTEM)
		function classdefSession:getComponent()
			return lookupAny[self].component
		end
	end
	----  internal class utilities
	--  credit to https://codereview.stackexchange.com/questions/90177/get-file-name-with-extension-and-get-only-extension
	--  for source example
	local function GetFileName(url, noExtension)
		if noExtension == true then
			return url:gsub(url:match("^.+(%..+)$"), ""):match("^.+/(.+)$")
		end
		return url:match("^.+/(.+)$")
	end
		
	local function GetFileExtension(url)
		return GetFileName(url):match("^.+(%..+)$")
	end
	----  API declaration
	--  @param  string pathToSource,  boolean runNow
	--  @desc  Initializes a Component on Creation
	--  @returns  Object Component
	function __private_Component:onCreate(pathToSource, runNow, ...)
		local sourceName = GetFileName(
			pathToSource,
			true
		)
		local sourceExtension = GetFileExtension(
			pathToSource
		)
		assert(sourceExtension == ".lua", "bad argument #1 (unsupported extension ".. sourceExtension .." (only supports .lua!)")
		assert(lookupComponent[sourceName] == nil, "bad argument #1 (Component ".. sourceName .." is already loaded into PigBot)")
		------  LOGIC
		----  variable declaration/initialization
		lookupAny.Object.onCreate(self)
		local __private_Component = lookupAny[self]
		__private_Component.objectRoot.name = sourceName
		__private_Component.componentRoot = setmetatable(
			{},
			{
				__metatable = false
			}
		)
		__private_Component.componentName = sourceName
		__private_Component.componentExtension = sourceExtension
		ObjectsManager.newObject("Session", nil, self)
		local __private_Session = lookupAny[__private_Component.session]
		__private_Session.environment.root = __private_Component.objectRoot
		--  initialize component
		if (sourceName == "System") then
			local active = __private_Session.activeThreads
			active[#active + 1] = getRunningThread()
			function __private_Component.objectRoot.run() end
			setfenv(
				3,
				__private_Session.environment
			)
			setfenv(
				2,
				__private_Session.environment
			)
		else
			-- load the sourceString from the file
			local sourceString, failure = fsys.readFile(
				pathToSource,
				0666
			)
			if (sourceString == nil) then
				error("cannot initialize ".. sourceName ..": source read error >>\n".. failure)
			end
			--  compile the source into a callable function
			local callable, failure = loadstring(sourceString)
			if callable == nil then
				error("cannot initialize ".. sourceName ..": source compile error >>\n".. failure)
			end
			--  finalize initialization prep
			__private_Component.thread = createThread(
				setfenv(
					callable,
					__private_Session.environment
				)
			)
			local attached = __private_Session.activeThreads
			attached[#attached + 1] = __private_Component.thread
			lookupThreadToSession[__private_Component.thread] = __private_Component.session
			__private_Session.environment.PBCore = ObjectsManager.newObject("PBCore")
			lookupComponent[sourceName] = self
			--  start thread if runNow is true
			if (runNow == true) then
				return resumeThread(
					__private_Component.thread,
					...
				)
			end
		end
	end
	--  @param  tuple arguments
	--  @desc  Runs the Component thread, passing the tuples if any
	--  @returns  tuple
	function classdefComponent:run(...)
		local __private_Component = lookupAny[self]
		debug(
			"running Component:",
			self.classname,
			self.name
		)
		local ran, failure = coroutine.resume(
			__private_Component.thread,
			...
		)
		if (ran == false) then
			return error("failed to run ".. self.classname .." ".. self.name .." >>\n".. failure)
		end
		classdefPBCore[__private_Component.componentName] = __private_Component.componentRoot
	end
	--  @param  none
	--  @desc  Returns the status of the Component
	--  @returns  string
	function classdefComponent:getStatus()
		return getThreadStatus(lookupAny[self].thread)
	end
	--  @param  none
	--  @desc  Returns a proxy table containing all the identities for the session
	--  @returns  table
	function classdefComponent:getPermissions()
		return lookupAny[lookupAny[self].session]:getPermissions()
		-- return {
		-- 	read = permissions.read/1,
		-- 	write = permissions.write/1,
		-- 	execute = permissions.execute/1
		-- }
	end
end
----  @classdef  Library
----  @desc  An API-access limited Component for declaring Library-based APIs
local classdefLibrary = ObjectsManager.newObjectClass(
	"Library", 
	classdefComponent
) do
	local __private_Library = lookupAny.Library
	------  INITIALIZATION
	----  variable declaration
	local totalLibraries = setmetatable(
		{},
		{
			__mode = "v"
		}
	) __private_PBCore.totalLibraries = totalLibraries
	local loadedLibraries = setmetatable(
		{},
		{
			__mode = "k",
			__newindex = function(self, index, newindex)
				totalLibraries[#totalLibraries + 1] = newindex
				rawset(
					self, 
					index, 
					newindex
				)
			end
		}
	)
end
----  @classdef  Service
----  @desc  Implements critical APIs and system features
local classdefService = ObjectsManager.newObjectClass(
	"Service", 
	classdefComponent
) do
	local __private_Service = lookupAny.Service
	------  INITIALIZATION222
	----  variable declaration
	local totalServices = setmetatable(
		{},
		{
			__mode = "v"
		}
	)  __private_PBCore.totalServices = totalServices
	local loadedServices = setmetatable(
		{},
		{
			__mode = "v",
			__newindex = function(self, index ,value)
				totalServices[#totalServices + 1] = index
				rawset(
					self,
					index,
					value
				)
			end
		}
	)  __private_PBCore.loadedServices = loadedServices
end
----  @classdef  Feature
----  @desc  Implements features which require less API access and are more controlled
local classdefFeature = ObjectsManager.newObjectClass(
	"Feature", 
	classdefComponent
) do
	local __private_Feature = lookupAny.Feature
	------  INITIALIZATION
	----  variable declaration
	__private_PBCore.totalFeatures = setmetatable(
		{},
		{
			__mode = "v"
		}
	) local totalFeatures = __private_PBCore.totalFeatures
	__private_PBCore.loadedFeatures = setmetatable(
		{},
		{
			__mode = "v",
			__newindex = function(self, index ,value)
				totalFeatures[#totalFeatures + 1] = index
				rawset(
					self,
					index,
					value
				)
			end
		}
	) local loadedFeatures = __private_PBCore.loadedFeatures
end
----  PBCore API
--  @def  table  Info
--  @desc  Reference table containing PigBot version/build information
--  @permissions:
--  read: 		1 (FEATURE)
--  write: 		4 (NONWRITABLE)
--  execute:	4 (NONEXECUTABLE)
--  @param  string componentName
--  @desc  Waits for the Module to be ready, then returns its root
--  @returns  table
function classdefPBCore:depend(componentName)
	debug("waiting on dependency", componentName)
	local component = lookupComponent[componentName]
	while (component == nil) do
		sleep(250)
		component = lookupComponent[componentName]
	end
	local __private_Component = lookupAny[component]
	local __private_Session = lookupAny[__private_Component.session]
	while (__private_Session.environment.ready ~= nil) do
		sleep(250)
	end
	debug("got dependency", component)
	return component
end
----  expose Construct APIs (allowing permissions contro112)
classdefPBCore.newObject = ObjectsManager.newObject
classdefPBCore.newObjectClass = ObjectsManager.newObjectClass
--  @param  Object Object
--  @desc  Used to lookup the Private data of an Object. This function is intended for
--  class building.
function classdefPBCore:lookupObject(Object)
	if (ObjectsManager.isManaged(Object) == false) then
		return
	end
	return lookupAny[Object]
end
------  COMPONENT LOADING
ObjectsManager.newObject(
	"Service",
	"System",
	fsRoot .."System.lua"
)
ObjectsManager.newObject(
	"Library",
	"EnumsManager",
	fsRoot .."Components/Libraries/EnumsManager.lua"
)
ObjectsManager.newObject(
	"Library",
	"Utilities",
	fsRoot .."Components/Libraries/Utilities.lua"
)
ObjectsManager.newObject(
	"Service",
	"ThreadManager",
	fsRoot .."Components/Services/ThreadManager.lua"
)
ObjectsManager.newObject(
	"Library",
	"SignalsManager",
	fsRoot .."Components/Libraries/SignalsManager.lua"
)
ObjectsManager.newObject(
	"Service",
	"Logging",
	fsRoot .."Components/Services/Logging.lua"
)
----  run our Components
for index = 1, #totalComponents do
	totalComponents[index]:run()
end 
----  determine type of deployment
if (PBInfo.branch:sub(0, 5) == "indev") then
	--  run our general API tests
	for key, file in next, fsys.scandir(fsRoot .."tests") do
		if (file.name:sub(0, 7) == "apitest") then
			local source, failure = fsys.readFile(fsRoot .."tests/".. name)
			loadstring(source)()
		end
	end 
	--  [soonTM] implement specific tests such as permissions
	debug("tests pass")
end
if (PBInfo.deployType == "test") then
	exitProcess()
else
	--  perform initialization of System
	print("IMPLEMENT INITIALIZATION")
	exitProcess()
end
end))
if (ran == false) then
	error(failureResponse)
end