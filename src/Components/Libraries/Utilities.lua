--  TODO:  implement customized Debugging library
------  INITIALIZATION
----  variable and library localization
local find = string.find
local sub = string.sub
local libJSON = require("json")
local libFS = require("coro-fs")
local PBCore = PBCore
local newObject = PBCore.newObject
local newObjectClass = PBCore.newObjectClass
local EnumsManager = PBCore:depend("EnumsManager")
local Enums = EnumsManager.Enums
local root = root
local objectData = objectData environment.objectData = nil
local lookupAny = objectData.lookupAny
local pathJoin = require("pathJoin")
----  Utilities API
--  @ref  JSON
root.JSON = libJSON
--  @ref  FS
root.FileSystem = libFS
----  Enums
--  @Enum  Failure
--  @desc  Container for all possible failures
newObject(
    "Enum",
    "Failure",
    {
        SchedulerTaskFailed = "[%s] A scheduled task failed to run!\nfailure: %s",
        LuaSignalThreadResumeFailure = "[%s] Suspended thread %s failed to resume when firing LuaSignal %s\nfailure: %s",
        ClassNotCreatable = "Class %s is not intended to be Instanciated by Construct.new"
    }
)
----  Function API
--  @param  table target,  vararg ...
--  @desc  Assigns the vararg to target
--  @returns  target
function root.assign(target, ...)
    for i = 1, select("#", ...) do
        for key, value in next, select(i, ...) do
            target[key] = value
        end
    end
    return target
end  local assign = root.assign
--  @param  vararg ...
--  @desc  Merges the contents of vararg into a new table
--  @returns  table
function root.merge(...)
    local newTarget = {}
    assign(
        newTarget,
        ...
    )
    return newTarget
end
--  @param  string  toSplit,  string  delimiter
--  @desc  Splits toSplit into a table using delimitor
--  @returns  table
function root.splitString(str, delim)
    if (not str) or (not delim) or str == "" or delim == "" then
		return {}
	else
		local current = 1
		local result = { }
		while true do
			local start, finish = find(str, delim, current)
            if start and finish then
                result[#result + 1] = sub(str, current, start - 1)
				current = finish + 1
			else
				break
			end
        end
        result[#result + 1] = sub(str, current)
		return result
	end
end
ready()